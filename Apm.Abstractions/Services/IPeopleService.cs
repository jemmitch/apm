﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Apm.Model;

namespace Apm.Abstractions.Services
{
    public interface IPeopleService
    {
        Task<PersonModel> GetPersonByName(string name);

        Task<List<PersonModel>> GetAllPeople();
    }
}
