﻿namespace Apm.Model
{
    public class PersonModel
    {
        public string Name { get; set; }

        public string Dob { get; set; }

        public string BirthPlace { get; set; }

        public int Height { get; set; }

        public int Weight { get; set; }

        public int Age { get; set; }
    }
}
