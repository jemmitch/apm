﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Apm.Abstractions.Repository;
using Apm.Domain;
using CsvHelper;

namespace Apm.Repository.Csv
{
    public class CsvRepository : IRepository<Person>
    {
        private readonly string _filename;

        public CsvRepository(string filename)
        {
            _filename = filename;
        }

        public List<Person> GetAll()
        {
            using (var reader = new CsvReader(new StreamReader(_filename)))
            {
                reader.Configuration.HasHeaderRecord = true;
                reader.Configuration.IsHeaderCaseSensitive = false;
                return reader.GetRecords<Person>().ToList();
            }
        }

        public Person GetByName(string name)
        {
            return GetAll().FirstOrDefault(x => x.Name == name);
        }
    }
}
