﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apm.Abstractions.Services;
using Apm.Controllers;
using Apm.Model;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace Apm.UnitTests
{
    [TestFixture]
    public class PersonControllerTests
    {
        [Test]
        public async void CanGetAPerson()
        {
            // Arrange
            const string expectedName = "Tom Boonen";
            var peopleService = new Mock<IPeopleService>();
            peopleService.Setup(x => x.GetPersonByName(It.IsAny<string>())).Returns(Task.FromResult(new PersonModel { Name = expectedName }));
            
            var controller = new PersonsController(peopleService.Object);

            // Act
            var person = await controller.Get(expectedName);

            // Assert
            person.Name.Should().Be(expectedName);
        }

        [Test]
        public async void CanGetAllPeople()
        {
            // Arrange
            const string expectedName = "Tom Boonen";
            var peopleService = new Mock<IPeopleService>();
            peopleService.Setup(x => x.GetAllPeople()).Returns(Task.FromResult(new List<PersonModel> { new PersonModel { Name = expectedName } }));
            
            var controller = new PersonsController(peopleService.Object);

            // Act
            var people = await controller.Get();

            // Assert
            people.Count().Should().NotBe(0);
            var person = people.First();
            person.Name.Should().Be(expectedName);
        }
    }
}
