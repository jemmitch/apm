'use strict';

var myApp = angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/people', {templateUrl: '/Scripts/app/partials/people.html', controller: 'peopleController'});
  $routeProvider.when('/person/:Name', { templateUrl: '/Scripts/app/partials/person.html', controller: 'personController' });
  $routeProvider.otherwise({ redirectTo: '/people' });
}]);
