﻿using Apm.Domain;
using Apm.Model;
using AutoMapper;

namespace Apm.Mapping
{
    public class PersonProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Person, PersonModel>()
                .ForMember(x => x.Dob,
                    opt => opt.MapFrom(src => src.Dob.ToShortDateString()));
        }
    }
}
