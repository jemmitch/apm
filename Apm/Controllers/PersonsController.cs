﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Apm.Abstractions.Services;
using Apm.Model;

namespace Apm.Controllers
{
    public class PersonsController : ApiController
    {
        private readonly IPeopleService _peopleService;

        public PersonsController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        [HttpGet]
        public async Task<IEnumerable<PersonModel>> Get()
        {
            return await _peopleService.GetAllPeople();
        }

        [HttpGet]
        public async Task<PersonModel> Get(string name)
        {
            return await _peopleService.GetPersonByName(name);
        }
    }
}
