﻿using System;
using Apm.Domain;
using Apm.Mapping;
using Apm.Model;
using AutoMapper;
using FluentAssertions;
using NUnit.Framework;

namespace Apm.UnitTests
{
    [TestFixture]
    public class MappingTests
    {
        public MappingTests()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<PersonProfile>());

            Mapper.AssertConfigurationIsValid();    
        }

        [Test]
        public void CanMapADateOfBirth()
        {
            // Arrange
            const string expectedDob = "03/02/2001";
            var person = new Person {Dob = new DateTime(2001, 02, 03)};

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.Dob.Should().Be(expectedDob);
        }

        [Test]
        public void CanMapAge()
        {
            // Arrange
            const int expectedAge = 14;
            var person = new Person { Dob = new DateTime(2001, 02, 03) };

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.Age.Should().Be(expectedAge);
        }

        [Test]
        public void CanMapAName()
        {
            // Arrange
            const string expectedName = "TestName";
            var person = new Person { Name = expectedName };

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.Name.Should().Be(expectedName);
        }

        [Test]
        public void CanMapABirthPlace()
        {
            // Arrange
            const string birthPlace = "TestName";
            var person = new Person { BirthPlace = birthPlace };

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.BirthPlace.Should().Be(birthPlace);
        }

        [Test]
        public void CanMapAHeight()
        {
            // Arrange
            const int expectedHeight = 100;
            var person = new Person { Height = expectedHeight };

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.Height.Should().Be(expectedHeight);
        }

        [Test]
        public void CanMapAWeight()
        {
            // Arrange
            const int expectedWeight = 100;
            var person = new Person { Weight = expectedWeight };

            // Act
            var personModel = Mapper.Map<PersonModel>(person);

            // Assert
            personModel.Weight.Should().Be(expectedWeight);
        }
    }
}
