using System.Configuration;
using System.Web.Hosting;
using Apm.Abstractions.Repository;
using Apm.Abstractions.Services;
using Apm.Domain;
using Apm.Repository.Csv;
using Apm.Services;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace Apm
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            container.RegisterType<IRepository<Person>, CsvRepository>(new InjectionConstructor(HostingEnvironment.MapPath(ConfigurationManager.AppSettings["CsvFilePath"])));
            container.RegisterType<IPeopleService, PeopleService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}