﻿'use strict';

myApp.factory('dataservice', function ($http, $q) {
    return {
        getPeople: function () {
            var deferred = $q.defer();

            $http({ method: 'GET', url: '/api/persons/' })
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
          .error(function (data, status, headers, config) {
              deferred.reject(status);
          });
          return deferred.promise;
        },

        getPerson: function (name) {
            var deferred = $q.defer();

            $http({ method: 'GET', url: '/api/persons?name=' + name })
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
          .error(function (data, status, headers, config) {
              deferred.reject(status);
          });
          return deferred.promise;
        }
    }
});