﻿using System.Collections.Generic;

namespace Apm.Abstractions.Repository
{
    public interface IRepository<T>
    {
        List<T> GetAll();

        T GetByName(string name);
    }
}
