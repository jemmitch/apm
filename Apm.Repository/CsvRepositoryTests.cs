﻿using Apm.Repository.Csv;
using FluentAssertions;
using NUnit.Framework;

namespace Apm.IntegrationTests
{
    [TestFixture]
    public class CsvRepositoryTests
    {
        [Test]
        public void CanGetAllPeople()
        {
            // Arrange
            var repository = new CsvRepository("people.txt");

            // Act
            var people = repository.GetAll();

            // Assert
            people.Count.Should().NotBe(0);
        }

        [Test]
        public void CanGetAPersonByName()
        {
            // Arrange
            const string expectedName = "Tom Boonen";
            var repository = new CsvRepository("people.txt");

            // Act
            var person = repository.GetByName(expectedName);

            // Assert
            person.Should().NotBeNull();
            person.Name.Should().Be(expectedName);
        }
    }
}
