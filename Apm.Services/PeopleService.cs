﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Apm.Abstractions.Repository;
using Apm.Abstractions.Services;
using Apm.Domain;
using Apm.Model;
using AutoMapper;

namespace Apm.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly IRepository<Person> _personRepository;

        public PeopleService(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<PersonModel> GetPersonByName(string name)
        {
            return await Task.FromResult(Mapper.Map<PersonModel>(_personRepository.GetByName(name)));
        }

        public async Task<List<PersonModel>> GetAllPeople()
        {
            var list = Task.FromResult(_personRepository.GetAll()).Result;
            list.Sort((c1, c2) => c1.Name.CompareTo(c2.Name));

            return Mapper.Map<List<PersonModel>>(list);
        }
    }
}
