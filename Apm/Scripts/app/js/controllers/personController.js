﻿'use strict';

myApp.controller('personController',
    function personController($scope, dataservice, $location, $routeParams) {

        dataservice.getPerson($routeParams.Name).then(
              function (person) {
                  $scope.person = person;
              },
              function (statusCode) { console.log(statusCode); }
        );

        $scope.go = function (path) {
            $location.path(path);
        }
    })