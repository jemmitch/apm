﻿using System.Web.Mvc;

namespace Apm.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return File("/Scripts/app/index.html", "text/html");
        }
    }
}