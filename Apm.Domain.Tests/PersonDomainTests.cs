﻿using System;
using Apm.Domain;
using FluentAssertions;
using NUnit.Framework;

namespace Apm.UnitTests
{
    [TestFixture]
    public class PersonDomainTests
    {
        [Test]
        public void CanGetPersonsAge()
        {
            // Arrange / Act
            var person = new Person {Dob = new DateTime(2010, 1, 1)};

            // Assert
            person.Age.Should().Be(5);
        }
    }
}
