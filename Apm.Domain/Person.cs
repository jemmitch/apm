﻿using System;

namespace Apm.Domain
{
    public class Person
    {
        public string Name { get; set; }

        public DateTime Dob { get; set; }

        public string BirthPlace { get; set; }

        public int Height { get; set; }

        public int Weight { get; set; }

        public int Age
        {
            get
            {
                var today = DateTime.Today;
                var age = today.Year - Dob.Year;
                if (Dob > today.AddYears(-age)) age--;
                return age;
            }
        }
    }
}
