﻿using System.Collections.Generic;
using System.Linq;
using Apm.Abstractions.Repository;
using Apm.Domain;
using Apm.Mapping;
using Apm.Services;
using AutoMapper;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace Apm.UnitTests
{
    [TestFixture]
    public class PersonServiceTests
    {
        public PersonServiceTests()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<PersonProfile>());

            Mapper.AssertConfigurationIsValid();    
        }

        [Test]
        public async void CanGetAPerson()
        {
            // Arrange
            const string expectedName = "Tom Boonen";
            var peopleRepository = new Mock<IRepository<Person>>();
            peopleRepository.Setup(x => x.GetByName(It.IsAny<string>())).Returns(new Person {Name = expectedName});
            var service = new PeopleService(peopleRepository.Object);

            // Act
            var person = await service.GetPersonByName(expectedName);

            // Assert
            person.Name.Should().Be(expectedName);
        }

        [Test]
        public async void CanGetAllPeople()
        {
            // Arrange
            const string expectedName = "Tom Boonen";
            var peopleRepository = new Mock<IRepository<Person>>();
            peopleRepository.Setup(x => x.GetAll()).Returns(new List<Person> { new Person {Name = expectedName} });
            var service = new PeopleService(peopleRepository.Object);

            // Act
            var people = await service.GetAllPeople();

            // Assert
            people.Count.Should().NotBe(0);
            var person = people.First();
            person.Name.Should().Be(expectedName);
        }

        [Test]
        public async void PeopleShouldBeSortedByName()
        {
            // Arrange
            const string expectedFirstName = "A";
            const string expectedLastName = "B";
            var peopleRepository = new Mock<IRepository<Person>>();
            peopleRepository.Setup(x => x.GetAll()).Returns(new List<Person>
            {
                new Person { Name = "B" },
                new Person { Name = "A" }
            });
            var service = new PeopleService(peopleRepository.Object);

            // Act
            var people = await service.GetAllPeople();

            // Assert
            people.Count.Should().NotBe(0);
            var firstPerson = people.First();
            firstPerson.Name.Should().Be(expectedFirstName);

            var lastPerson = people.Last();
            lastPerson.Name.Should().Be(expectedLastName);
        }
    }
}
