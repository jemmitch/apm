﻿'use strict';

myApp.controller('peopleController', function peopleController($scope, $http, $log, dataservice) {
    dataservice.getPeople().then(
          function (people) { $scope.people = people; },
          function (statusCode) { console.log(statusCode); }
    );
})